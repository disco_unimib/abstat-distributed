#! /bin/bash

relative_path=`dirname $0`
root=`cd $relative_path;pwd`

dataset=$1
ontology=$2
output_dir=$3
isDistributed=$4
propMin=$5
inf=$6
card=$7
shacl=$8

jar="$root/summarization-spark/target/summarization-spark-0.0.1-SNAPSHOT.jar"
ont_name=$(echo $ontology | sed 's/.*\///')
echo "jar: $jar"
echo "files: $ontology"
echo "dataset: $dataset"
echo "output_dir: $output_dir"


if [[ "$isDistributed" == "true" ]]; then
	cmd="spark2-submit --class it.unimib.disco.abstat.distributed.application.Summarization --master yarn --deploy-mode cluster --files $ontology $jar yarn-cluster $dataset $ont_name $output"
	eval $cmd
	echo "Distributed Summarization done"
else
	cmd="java -cp  $jar  it.unimib.disco.abstat.distributed.application.Summarization local[*]  $dataset $ontology $output_dir $propMin $inf $card $shacl"
	echo $cmd
	if eval $cmd; then
		cd $output_dir
		for dir in ./*
		do
		    if [[ "$dir" == "./spark-concepts" ]]; then
		    	out="count-concepts.json"    	
		    elif [[ "$dir" == "./spark-datatypes" ]]; then
		    	out="count-datatype.json"  
		    elif [[ "$dir" == "./spark-datatype-properties" ]]; then
		    	out="count-datatype-properties.json"  
		    elif [[ "$dir" == "./spark-object-properties" ]]; then
		    	out="count-object-properties.json"  
		    elif [[ "$dir" == "./spark-datatype-akp" ]]; then
		    	out="datatype-akp.txt"  
		    elif [[ "$dir" == "./spark-object-akp" ]]; then
		    	out="object-akp.txt"   
		    elif [[ "$dir" == "./spark-datatype-cardinalities" ]]; then
		    	out="patternCardinalities.txt" 
		    elif [[ "$dir" == "./spark-object-cardinalities" ]]; then
		    	out="patternCardinalities.txt" 
		    elif [[ "$dir" == "./spark-datatype-patternsLight" ]]; then
		    	out="patterns_splitMode_datatype.txt" 
		    elif [[ "$dir" == "./spark-object-patternsLight" ]]; then
		    	out="patterns_splitMode_object.txt" 
		    elif [[ "$dir" == "./spark-datatype-predictedCardinalities" ]]; then
		    	out="predictedCardinalities.txt" 
		    elif [[ "$dir" == "./spark-object-predictedCardinalities" ]]; then
		    	out="predictedCardinalities.txt" 
		    elif [[ "$dir" == "./spark-datatype-merged" ]]; then
		    	out="datatype-akp-allstats.json" 
		    elif [[ "$dir" == "./spark-object-merged" ]]; then
		    	out="object-akp-allstats.json" 
		    else 
		    	continue
		    fi
		     
		    cat $dir/* >> $out            # union of files
		    rm -r $dir                    # remove spark output directory
		done
		rm "datatype-akp.txt"
		rm "object-akp.txt"
		rm "patternCardinalities.txt"
		rm "patterns_splitMode_datatype.txt"
		rm "patterns_splitMode_object.txt"
		rm "predictedCardinalities.txt"
		
		echo "Summarization done"
	else
		echo "ERROR"
	fi
fi
