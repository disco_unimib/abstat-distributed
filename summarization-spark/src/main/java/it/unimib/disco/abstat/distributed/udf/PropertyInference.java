package it.unimib.disco.abstat.distributed.udf;

import java.util.HashSet;
import java.util.Set;

import org.apache.spark.sql.api.java.UDF2;


import it.unimib.disco.abstat.distributed.minimalization.PropertyGraph;


public class PropertyInference implements UDF2<String, String, Set<String>>{
	
	static final long serialVersionUID = 4024451101789741830L;
	private PropertyGraph graph;
	
	public PropertyInference(String ontology_path) {
	    graph = new PropertyGraph(ontology_path, false);
	}
	
	
	// Notice that the top Property is added here (it's not obatained from the property graph) in order to avoid 
	// to have topObjectProperty predicate in the datatype triples (incorrect using of a object property). Same for datatype properties.
	public Set<String> call(String property, String type) {
		Set<String> types = new HashSet<String>();

		Set<String> list = graph.superProperties(property);
		if (list == null) { // if typegraph does not contain the concept
			list = new HashSet<String>();
			list.add(property);
		}
		if (type.equals("datatype"))
			list.add("http://www.w3.org/2002/07/owl#topDataProperty");
		else
			list.add("http://www.w3.org/2002/07/owl#topObjectProperty");
		types.addAll(list);

		return types;
	}
	
}
