package it.unimib.disco.abstat.distributed.minimalization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;

import com.hp.hpl.jena.ontology.ConversionException;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;


/* This graph contains only concepts which are represented inside the ontology (explicit definitnion, subclassOf and equivalentClass statements). 
** Therefore external concepts are not added to the graph
*/
public class TypeGraph implements Serializable {
	
	private static final long serialVersionUID = -212583253328187025L;
	DirectedAcyclicGraph<String, DefaultEdge> graph = new DirectedAcyclicGraph<String, DefaultEdge>(DefaultEdge.class);
	static OntModel ontologyModel;
	
	public TypeGraph(String ontology, boolean  uniqueRoot) {
		ontologyModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM, null);
		ontologyModel.read(ontology, "RDF/XML");	
		populateVertex(true);
		populateEdges();
		if(uniqueRoot)
			forceUniqueRoot();
	}
	
////////////////////////////////// GRAPH POPULATION ///////////////////////////////////////////////////////
	
	private void populateVertex(boolean enrich) {
		if(enrich){
			enrichWithImplicitClassesDeclarations(RDFS.subClassOf);
			enrichWithImplicitClassesDeclarations(OWL.equivalentClass);
		}
		ExtendedIterator<OntClass> TempExtractedConcepts = ontologyModel.listClasses();
		while(TempExtractedConcepts.hasNext()) {
			String URI = TempExtractedConcepts.next().getURI();
			if(URI!=null && !URI.equals(""))
				graph.addVertex(URI);
		}
		TempExtractedConcepts.close();
	}
	
	
	private void populateEdges() {
		Iterator<OntClass> itConcepts = ontologyModel.listClasses();
		while(itConcepts.hasNext()) {
			OntClass concept = itConcepts.next();
			String v = concept.getURI();
			
			if( v!=null ){
				try{
					ExtendedIterator<OntClass> itSup = concept.listSuperClasses(true);
					while(itSup.hasNext()) {
						String vSup = itSup.next().getURI();
						if(vSup!=null )
							graph.addEdge(v, vSup);
					}
				}
				catch(ConversionException e){
					String queryString = "SELECT ?obj " +
										 "WHERE { <" + v + "> <http://www.w3.org/2000/01/rdf-schema#subClassOf>  ?obj }";

					Query query = QueryFactory.create(queryString) ;
					QueryExecution qexec = QueryExecutionFactory.create(query, ontologyModel) ;
					
					try {
						ResultSet results = qexec.execSelect();
					    
					    for ( ; results.hasNext() ; ){
					      QuerySolution soln = results.nextSolution() ;
					      String vSup = soln.getResource("obj").getURI();
					      if(vSup!=null && v!=vSup && !graph.containsVertex(vSup)) 
					    	  graph.addVertex(vSup);
					      graph.addEdge(v, vSup);
					    }
					} 
					finally { qexec.close() ; }
				}	
			}	
		}		
	}
	
	
	// extract classes from the subject and object of the statements with the input property
	private void enrichWithImplicitClassesDeclarations( final Property p) {
		StmtIterator statements = ontologyModel.listStatements(new SimpleSelector(){
			@Override
			public boolean selects(Statement s) {
				Property predicate = s.getPredicate();
				return predicate.equals(p);
			}
		});
		HashSet<String> implicitClasses = new HashSet<String>();
		while(statements.hasNext()){
			Statement statement = statements.next();
			implicitClasses.add(statement.getSubject().getURI());
			implicitClasses.add(statement.getObject().toString());
		}
		statements.close();
		for(String implicitClass : implicitClasses)
			ontologyModel.createClass(implicitClass);
	}
	
	
	// links all the orphans of the type graph to owl:Thing
	private void forceUniqueRoot() {
		String thing = OWL.Thing.getURI();
		if (!graph.containsVertex(thing))
			graph.addVertex(thing);
		for (String v : findRoots()) {
			if (!v.equals(thing)) 
				graph.addEdge(v, thing);
		}
	}
	
	
	////////////////////////////////// OPERATIONS ///////////////////////////////////////////////////////
	
	public List<List<String>> pathsBetween(String leaf, String root){
		ArrayList<List<String>> paths = new ArrayList<List<String>>();
		if(graph.containsVertex(leaf) && graph.containsVertex(root)){
			inOrderTraversal(leaf, root, new ArrayList<String>(), paths);
		}
		return paths;
	}
	
	
	private void inOrderTraversal(String leaf, String root, List<String> currentPath, List<List<String>> paths){
		ArrayList<String> path = new ArrayList<String>(currentPath);
		path.add(leaf);
		if(leaf.equals(root)){
			paths.add(path);
		}
		for(DefaultEdge edgeToSuperType : graph.outgoingEdgesOf(leaf)){
			String superType = graph.getEdgeTarget(edgeToSuperType);
			inOrderTraversal(superType, root, path, paths);
		}
	}
	

	// if v is present in the typegraph it returns the superconcepts (transitive closure) plus the input concept, otherwise return null
	public Set<String> superConcepts(String v) {
		if (!graph.containsVertex(v)) 
			return null;
		else {
			Set<String> set = new HashSet<String>();
			Set<DefaultEdge> outgoingEdges = graph.outgoingEdgesOf(v);

			for (DefaultEdge edge : outgoingEdges) {
				String padre = graph.getEdgeTarget(edge);
				String source = graph.getEdgeSource(edge);
				Set<String> gerarchia = superConcepts(padre);
				set.add(source);
				set.addAll(gerarchia);
			}
			if (outgoingEdges.isEmpty()) 
				set.add(v);
			return set;
		}
	}
	
	
	// returns a set of vertexes without father
	public HashSet<String> findRoots() {
		HashSet<String> orphans = new HashSet<String>();

		for (String vertex : graph.vertexSet()) {
			boolean isOrphan = true;
			for (DefaultEdge edge : graph.edgesOf(vertex)) {
				if (graph.getEdgeSource(edge).equals(vertex))
					isOrphan = false;
			}
			if (isOrphan)
				orphans.add(vertex);
		}
		return orphans;
	}
	
	
	// given a set of types it returns a set with only minimal types
	public Set<String> minimalize(Set<String> set) {
		Set<String> minimalTypes = new HashSet<String>();
		String firstMinimalType = (String) set.toArray()[0];
		minimalTypes.add(firstMinimalType);
		set.remove(firstMinimalType);
		
		boolean minimal= true;
		for(String concept: set) {
			Iterator<String> it = minimalTypes.iterator();
			while(it.hasNext()) {
				String minimalType = it.next();
				if(!pathsBetween(minimalType, concept).isEmpty())
					minimal = false;
				if(!pathsBetween(concept, minimalType).isEmpty())
					it.remove();
			}
			if(minimal)
				minimalTypes.add(concept);
			minimal = true;
		}
		return minimalTypes;
	}

}
