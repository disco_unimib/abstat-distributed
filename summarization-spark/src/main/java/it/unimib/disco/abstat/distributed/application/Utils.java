package it.unimib.disco.abstat.distributed.application;

import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.concat;
import static org.apache.spark.sql.functions.count;
import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.max;
import static org.apache.spark.sql.functions.min;
import static org.apache.spark.sql.functions.round;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class Utils {

	SparkSession session;
	String output_dir;
	
	public Utils(SparkSession session, String output_dir) {
		this.session = session;	
		this.output_dir = output_dir;
	}
	
	
	public void verifyTypes(String entity) {
		/* print types */
		session.sql("SELECT subject, object " + 
					"FROM dataset "  + 
					"WHERE type = 'typing' AND subject = \"http://dbpedia.org/resource/Katarina_Kresal\" "
				   ).show(200,false);
	}
	
	
	public void verifyMinimalTypes(String entity) {
		/* minimal types per entity */
		session.sql("SELECT subject AS entity, minimalize(object) AS minimalTypes " + 
					"FROM typing_triples " + 
					"GROUP BY subject"
					).createOrReplaceTempView("mTypes_dataset");
		
		/* print minimal types */
		session.sql("SELECT entity, explode(minimalTypes) AS minimalType " + 
					"FROM mTypes_dataset " + 
					"WHERE entity = \"" + entity +"\" "
					).show(200,false);
	}
	
	
	public void verifyDatatypeAKP(String akp) {
		String[] splitted = akp.split("##");
		/* calculate minimal types for each subject */
		session.sql("SELECT subject, " + 
					"   CASE" + 
					"      WHEN minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE minimalType " + 
					"   END AS subj_Type, " +
					"predicate, object, datatype AS obj_Type " +
					"FROM " + 
					"   mTypes_dataset " +
					"   RIGHT OUTER JOIN " + 
					"      datatype_triples " + 
					"      ON mTypes_dataset.entity = datatype_triples.subject "
					).createOrReplaceTempView("datatype_akp");
		
		/* group by AKP and calculate freq */
		session.sql("SELECT subject, subj_Type, predicate, object, obj_Type  " +
					"FROM datatype_akp " + 
					"WHERE subj_Type = \"" + splitted[0] + "\" AND predicate = \"" + splitted[1] + "\" AND obj_Type = \"" + splitted[2] + "\" "
					).show(200, false);
	}
	
	
	public void verifyObjectAkp(String akp) {
		String[] splitted = akp.split("##");
		
		/* calculate minimal types for each subject */
		session.sql("SELECT subject, " + 
					"   CASE" + 
					"      WHEN minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE minimalType " + 
					"   END AS subj_Type, " +
					"predicate, object " + 
					"FROM " + 
					"   mTypes_dataset " +
					"   RIGHT OUTER JOIN " + 
					"      object_triples " + 
					"      ON mTypes_dataset.entity = object_triples.subject "
					).createOrReplaceTempView("object_akp");
			
		/* calculate minimal types for each object */
		session.sql("SELECT subject, subj_Type, predicate, " + 
					"   CASE" + 
					"      WHEN minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE minimalType " + 
					"   END AS obj_Type, object " +
					"FROM " + 
					"   mTypes_dataset " +
					"   RIGHT OUTER JOIN " + 
					"      object_akp " + 
					"      ON mTypes_dataset.entity = object_akp.object "
				).createOrReplaceTempView("object_akp");
		
		/* group by AKP and calculate freq */
		session.sql("SELECT subject, subj_Type, predicate, obj_Type, object  " +
					"FROM object_akp " + 
					"WHERE subj_Type = \"" + splitted[0] + "\" AND predicate = \"" + splitted[1] + "\" AND obj_Type = \"" + splitted[2] + "\" " 
					).show(200, false);
	}
	
	
	// this version enforce a persist in the middle of the join.
	public void calculateObjectCardinalitiesLight() {
		 session.sql("SELECT " + 
				"   CASE" + 
				"      WHEN mts.minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE mts.minimalType " + 
				"   END AS subj_Type, " +
				"   t.subject, t.predicate, t.object " + 
				"FROM " + 
				"   mTypes_dataset mts " +
				"   RIGHT OUTER JOIN object_triples t ON mts.entity = t.subject " 
			).persist().createOrReplaceTempView("temp");
			
		 Dataset<Row> grezzo =  session.sql("SELECT " + 
					"  tmp.subj_Type, tmp.subject, tmp.predicate, tmp.object, " + 
					"   CASE" + 
					"      WHEN mto.minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE mto.minimalType " + 
					"   END AS obj_Type " +
					"FROM " + 
					"   temp tmp" +
					"   LEFT OUTER JOIN mTypes_dataset mto ON mto.entity = tmp.object "
				)
		.withColumn("AKP", concat(col("subj_Type"), lit("##"),col("predicate"), lit("##"), col("obj_Type")) )
		.select("AKP", "subject", "object");
		
		grezzo.groupBy("subject", "AKP").agg(count("subject").alias("count")).
		groupBy("AKP").agg(max("count").alias("max"), round(avg("count")).cast("integer").alias("avg"), min("count").alias("min"))
		.createOrReplaceTempView("object_cardinalities2");
		
		grezzo.groupBy("object", "AKP").agg(count("object").alias("count")).
		groupBy("AKP").agg(max("count").alias("max"), round(avg("count")).cast("integer").alias("avg"), min("count").alias("min"))
		.createOrReplaceTempView("object_cardinalities1");
		
		session.sql("SELECT c1.AKP, c1.max, c1.avg, c1.min, c2.max AS max2, c2.avg AS avg2, c2.min AS min2 from object_cardinalities1 c1 JOIN object_cardinalities2 c2 ON c1.AKP = c2.AKP")
		.write().option("sep", ";").csv(output_dir + "/spark-object-cardinalities-light");		
	}	

}
