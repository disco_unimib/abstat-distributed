package it.unimib.disco.abstat.distributed.application;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.storage.StorageLevel;

import com.hp.hpl.jena.vocabulary.RDFS;

import static org.apache.spark.sql.functions.*;

import it.unimib.disco.abstat.distributed.minimalization.Triple;
import it.unimib.disco.abstat.distributed.udf.Inference;
import it.unimib.disco.abstat.distributed.udf.Minimalize;
import it.unimib.disco.abstat.distributed.udf.PredictCardinalities;
import it.unimib.disco.abstat.distributed.udf.PropertyInference;
import it.unimib.disco.abstat.distributed.udf.PropertyMinimalization;

public class Summarization {
	
	private SparkSession session;
	private String output_dir;
	private String[] datasets;
	private String ontology;
	private boolean minProp;
	private boolean inference;
	private boolean cardinality;
	private boolean shacl;


	public Summarization(String master, String datasets, String ontology, String output_dir, boolean minProp, boolean inference, boolean cardinality, boolean shacl) {
		session = SparkSession.builder().appName("ABSTAT-spark").master(master).getOrCreate();
		this.output_dir = output_dir;
		this.datasets = datasets.split(",");
		this.ontology = ontology;
		this.minProp = minProp;
		this.inference = inference;
		this.cardinality = cardinality;
		this.shacl = shacl;
	}
	
	
	public static void main(String[] args) throws Exception {
		Summarization s = new Summarization(args[0],args[1], args[2], args[3], Boolean.valueOf(args[4]), Boolean.valueOf(args[5]), Boolean.valueOf(args[6]), Boolean.valueOf(args[7]));
		
		s.preProcessing();
		s.countConcepts();
		s.countDatatypes();
		s.countProperties();
		
		if(s.minProp)
			s.propertyMinimalization();
		
		s.calculateMinimalTypes();
		s.calculateDatatypeAKP();
		s.calculateObjectAKP();	
		
		if(s.inference) {
			s.calculateInferredTypes();
			s.calculateDatatypeInference();;
			s.calculateObjectInference();
		}
		if(s.cardinality) {
			s.calculateDatatypeCardinalities();
			s.calculateObjectCardinalities();
		}
		if(s.shacl)
			s.predictCardinalities();
		
		s.buildJSON("datatype");
		s.buildJSON("object");
	}
	

	public void preProcessing() throws Exception {
		if(datasets[0].contains("parquet"))
			session.read().parquet(datasets).createOrReplaceTempView("dataset");
		else {
			JavaRDD<String> input = session.read().textFile(datasets).javaRDD();
			JavaRDD<Triple> rdd = new Splitter().calculate(input);
			Dataset<Row> data = session.createDataFrame(rdd, Triple.class);
			data.createOrReplaceTempView("dataset");
		}
		regUDF(ontology);
		split(true, minProp);	
	}
	
	
	public void regUDF(String ontology) throws Exception {
		new Minimalize(ontology, session).register();
		new PredictCardinalities(session).register();
		new PropertyMinimalization(ontology, session).register();
		new Inference(ontology,session).register();
		session.udf().register("propInference", new PropertyInference(ontology), DataTypes.createArrayType(DataTypes.StringType));
	}
	
	
	public void split(boolean unique, boolean minProp) {
		String distinct ="";
		if(unique) 
			distinct = " DISTINCT";
		
		/* isolate typing asserts */
		session.sql("SELECT " + distinct + " subject, object " + 
					"FROM dataset "  + 
					"WHERE type = 'typing'"
				   ).createOrReplaceTempView("typing_triples");	
		
		/* isolate object relation asserts */
		String q1 = "SELECT " + distinct + " subject, predicate, object " + 
				"FROM dataset "  + 
				"WHERE type = 'object_relational'" ;
		
		/* isolate datatype relation asserts */
		String q2 = "SELECT " + distinct + " subject, predicate, object, " +                     
				"   CASE" + 
				"      WHEN datatype IS NULL THEN 'http://www.w3.org/2000/01/rdf-schema#Literal' " + 
				"      ELSE datatype " + 
				"   END AS datatype " +
				"FROM dataset "  + 
				"WHERE type = 'dt_relational'" ;
		
		if(minProp) {
			session.sql(q1).createOrReplaceTempView("object_triples");
			session.sql(q2).createOrReplaceTempView("datatype_triples");
		}
		else {
			session.sql(q1).persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("object_triples");
			session.sql(q2).persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("datatype_triples");
		}
	}
	
	
	// note that this implementation of property minimalization implies the elimination of duplicates 
	public void propertyMinimalization() {
	
		/* isolate object relation asserts */
		session.sql("SELECT subject, object, minprop(predicate) AS minProperties " + 
				"FROM object_triples " + 
				"GROUP BY subject, object"
				).createOrReplaceTempView("object_triples");
		
		session.sql("SELECT subject, explode(minProperties) AS predicate, object " + 
					"FROM object_triples"
					).persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("object_triples");	

		/* isolate datatype relation asserts */
		session.sql("SELECT subject, object, minprop(predicate) AS minProperties, datatype " +
				"FROM datatype_triples "  + 
				"GROUP BY subject, object, datatype "
			   ).createOrReplaceTempView("datatype_triples");
		
		session.sql("SELECT subject, explode(minProperties) AS predicate, object, datatype " + 
				    "FROM datatype_triples"
				    ).persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("datatype_triples");	
	}
	
	

	// calculates concept occurrences
	public void countConcepts() {
		session.sql("SELECT object AS resource, COUNT(*) AS occ FROM typing_triples GROUP BY object")
		.write().json(output_dir + "/spark-concepts");
	}
	
	// calculates datatype occurrences
	public void countDatatypes() {
		session.sql("SELECT datatype AS resource, COUNT(*) AS occ FROM datatype_triples GROUP BY datatype")
		.write().json(output_dir + "/spark-datatypes");
	}
	
	// calculates predicate occurrences
	public void countProperties() {
		session.sql("SELECT predicate AS resource, COUNT(*) AS occ FROM datatype_triples GROUP BY predicate")
		.write().json(output_dir + "/spark-datatype-properties");
		session.sql("SELECT predicate AS resource, COUNT(*) AS occ FROM object_triples GROUP BY predicate")
		.write().json(output_dir + "/spark-object-properties");
	}

	
	public void calculateMinimalTypes() {
		/* minimal types per entity */
		session.sql("SELECT subject AS entity, minimalize(object) AS minimalTypes " + 
					"FROM typing_triples " + 
					"GROUP BY subject"
					).createOrReplaceTempView("mTypes_dataset");
		
		/* minimal type per entity */
		session.sql("SELECT entity, explode(minimalTypes) AS minimalType " + 
					"FROM mTypes_dataset"
					).persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("mTypes_dataset");
	}

	
	////////////////////////////////////////////////// AKPs ///////////////////////////////////////////////////////////////////
	
	public void calculateDatatypeAKP() {
		/* calculate minimal types for each subject */
		session.sql("SELECT " + 
					"   CASE" + 
					"      WHEN minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE minimalType " + 
					"   END AS subj_Type, " +
					"predicate, datatype AS obj_Type " +
					"FROM " + 
					"   mTypes_dataset " +
					"   RIGHT OUTER JOIN " + 
					"      datatype_triples " + 
					"      ON mTypes_dataset.entity = datatype_triples.subject "
					).createOrReplaceTempView("datatype_akp");
		
		/* group by AKP and calculate freq */
		session.sql("SELECT subj_Type, predicate, obj_Type, COUNT(*) AS freq  " +
					"FROM datatype_akp " + 
					"GROUP BY subj_Type, predicate, obj_Type "
					).write().option("sep", ";").csv(output_dir + "/spark-datatype-akp");
	}
	
	
	public void calculateObjectAKP() {
		/* calculate minimal types for each subject */
		session.sql("SELECT" + 
					"   CASE" + 
					"      WHEN minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE minimalType " + 
					"   END AS subj_Type, " +
					"predicate, object " + 
					"FROM " + 
					"   mTypes_dataset " +
					"   RIGHT OUTER JOIN " + 
					"      object_triples " + 
					"      ON mTypes_dataset.entity = object_triples.subject "
					).createOrReplaceTempView("object_akp");
			
		/* calculate minimal types for each object */
		session.sql("SELECT subj_Type, predicate, " + 
					"   CASE" + 
					"      WHEN minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
					"      ELSE minimalType " + 
					"   END AS obj_Type " +
					"FROM " + 
					"   mTypes_dataset " +
					"   RIGHT OUTER JOIN " + 
					"      object_akp " + 
					"      ON mTypes_dataset.entity = object_akp.object "
				).createOrReplaceTempView("object_akp");
		
		/* group by AKP and calculate freq */
		session.sql("SELECT subj_Type, predicate, obj_Type, COUNT(*) AS freq  " +
					"FROM object_akp " + 
					"GROUP BY subj_Type, predicate, obj_Type "
					).write().option("sep", ";").csv(output_dir + "/spark-object-akp");
	}
	
	////////////////////////////////////////////////// CARDINALITIES ///////////////////////////////////////////////////////////////////
	
	
	public void calculateDatatypeCardinalities() {
		Dataset<Row> grezzo = session.sql("SELECT " + 
				"   CASE" + 
				"      WHEN mts.minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE mts.minimalType " + 
				"   END AS subj_Type, " +
				"   t.subject, t.predicate, t.object, t.datatype " + 
				"FROM " + 
				"   mTypes_dataset mts" +
				"   RIGHT OUTER JOIN datatype_triples t ON mts.entity = t.subject "
			)
		.withColumn("AKP", concat(col("subj_Type"), lit("##"),col("predicate"), lit("##"), col("datatype")) )
		.select("AKP", "subject", "object");
		
		this.session.catalog().uncacheTable("datatype_triples");
		
		grezzo.groupBy("subject", "AKP").agg(count("subject").alias("count")).
		groupBy("AKP").agg(max("count").alias("max"), floor(avg("count")).cast("integer").alias("avg"), min("count").alias("min"))
		.createOrReplaceTempView("datatype_cardinalities2");
		
		grezzo.groupBy("object", "AKP").agg(count("object").alias("count")).
		groupBy("AKP").agg(max("count").alias("max"), floor(avg("count")).cast("integer").alias("avg"), min("count").alias("min"))
		.createOrReplaceTempView("datatype_cardinalities1");
		
		session.sql("SELECT c1.AKP, c1.max, c1.avg, c1.min, c2.max AS max2, c2.avg AS avg2, c2.min AS min2 from datatype_cardinalities1 c1 JOIN datatype_cardinalities2 c2 ON c1.AKP = c2.AKP")
		.persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("datatype_cardinalities");
		
		session.sql("SELECT * FROM datatype_cardinalities").write().option("sep", ";").csv(output_dir + "/spark-datatype-cardinalities");
	}	
	

	public void calculateObjectCardinalities() {
		Dataset<Row> grezzo = session.sql("SELECT " + 
				"   CASE" + 
				"      WHEN mts.minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE mts.minimalType " + 
				"   END AS subj_Type, " +
				"   t.subject, t.predicate, t.object, " + 
				"   CASE" + 
				"      WHEN mto.minimalType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE mto.minimalType " + 
				"   END AS obj_Type " +
				"FROM " + 
				"   mTypes_dataset mts" +
				"   RIGHT OUTER JOIN object_triples t ON mts.entity = t.subject " +
				"   LEFT OUTER JOIN mTypes_dataset mto ON mto.entity = t.object " 
			)
		.withColumn("AKP", concat(col("subj_Type"), lit("##"),col("predicate"), lit("##"), col("obj_Type")) )
		.select("AKP", "subject", "object");
		
		this.session.catalog().uncacheTable("object_triples");
		this.session.catalog().uncacheTable("mTypes_dataset");
		
		grezzo.groupBy("subject", "AKP").agg(count("subject").alias("count")).
		groupBy("AKP").agg(max("count").alias("max"), floor(avg("count")).cast("integer").alias("avg"), min("count").alias("min"))
		.createOrReplaceTempView("object_cardinalities2");
		
		grezzo.groupBy("object", "AKP").agg(count("object").alias("count")).
		groupBy("AKP").agg(max("count").alias("max"), floor(avg("count")).cast("integer").alias("avg"), min("count").alias("min"))
		.createOrReplaceTempView("object_cardinalities1");
		
		session.sql("SELECT c1.AKP, c1.max, c1.avg, c1.min, c2.max AS max2, c2.avg AS avg2, c2.min AS min2 from object_cardinalities1 c1 JOIN object_cardinalities2 c2 ON c1.AKP = c2.AKP")
		.persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("object_cardinalities");	
		
		session.sql("SELECT * FROM object_cardinalities").write().option("sep", ";").csv(output_dir + "/spark-object-cardinalities");
	}	

	
	////////////////////////////////////////////////// INFERENCE ///////////////////////////////////////////////////////////////////
	
	public void calculateInferredTypes() {	
		session.sql("SELECT entity, inference(minimalType) AS inferredTypes " + 
					"FROM mTypes_dataset " + 
					"GROUP BY entity"
					).createOrReplaceTempView("inferredTypes_dataset");
		
		session.sql("SELECT entity, explode(inferredTypes) AS inferredType " + 
					"FROM inferredTypes_dataset"
					).persist(StorageLevel.MEMORY_ONLY()).createOrReplaceTempView("inferredTypes_dataset");	
	}
	
	
	public void calculateInferredProperties(String type) {		
		session.sql("SELECT DISTINCT predicate FROM " + type + "_triples").withColumn("type", lit(type))
		.createOrReplaceTempView("properties_inf");
		
		session.sql("SELECT DISTINCT predicate, propInference(predicate, type) AS superPredicates FROM properties_inf ")
		.createOrReplaceTempView("properties_inf");
		
		session.sql("SELECT predicate, explode(superPredicates) AS superPredicate FROM properties_inf")
		.createOrReplaceTempView("properties_inf");	
	}
	
	
	public void calculateDatatypeInference() {		
		session.sql("SELECT" + 
				"   CASE" + 
				"      WHEN inferredType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE inferredType " + 
				"   END AS subj_Type, " +
				"predicate, datatype AS obj_Type " + 
				"FROM " + 
				"   inferredTypes_dataset " +
				"   RIGHT OUTER JOIN " + 
				"      datatype_triples " + 
				"      ON inferredTypes_dataset.entity = datatype_triples.subject "
			).withColumn("temp", when(col("obj_Type").equalTo(RDFS.Literal.getURI()), functions.array( col("obj_Type")))
				       .otherwise(functions.array( col("obj_Type"), lit(RDFS.Literal.getURI()) )))
		     .drop("obj_Type")
			 .withColumn("exploded", explode(col("temp"))).drop("temp")
			 .withColumnRenamed("exploded","obj_Type").createOrReplaceTempView("datatype_patterns");
		
		
		/* group by AKP and calculate freq */
		session.sql("SELECT subj_Type, predicate, obj_Type, COUNT(*) AS freq  " +
					"FROM datatype_patterns " + 
					"GROUP BY subj_Type, predicate, obj_Type "
				).createOrReplaceTempView("datatype_patterns");
		
		propertyInference("datatype");
	}
	
	
	public void calculateObjectInference() {	
		session.sql("SELECT" + 
				"   CASE" + 
				"      WHEN inferredType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE inferredType " + 
				"   END AS subj_Type, " +
				"predicate, object " + 
				"FROM " + 
				"   inferredTypes_dataset " +
				"   RIGHT OUTER JOIN " + 
				"      object_triples " + 
				"      ON inferredTypes_dataset.entity = object_triples.subject "
			).createOrReplaceTempView("object_patterns");
		
		session.sql("SELECT subj_Type, predicate, " + 
				"   CASE" + 
				"      WHEN inferredType IS NULL THEN 'http://www.w3.org/2002/07/owl#Thing' " + 
				"      ELSE inferredType " + 
				"   END AS obj_Type " +
				"FROM " + 
				"   inferredTypes_dataset " +
				"   RIGHT OUTER JOIN " + 
				"      object_patterns " + 
				"      ON inferredTypes_dataset.entity = object_patterns.object "
			).createOrReplaceTempView("object_patterns");
	
		session.catalog().uncacheTable("inferredTypes_dataset");
		
		
		/* group by patterns and calculate number of instances */
		session.sql("SELECT subj_Type, predicate, obj_Type, COUNT(*) AS freq  " +
				"FROM object_patterns " + 
				"GROUP BY subj_Type, predicate, obj_Type "
			).createOrReplaceTempView("object_patterns");
		
		propertyInference("object");
	}
	

	
	public void propertyInference(String type) {
		calculateInferredProperties(type); 
		
		session.sql("SELECT subj_Type, superPredicate, obj_Type, SUM(freq) as numInstances "+ 
				"FROM " + 
				"   properties_inf " +
				"   JOIN " + 
				"      "+ type + "_patterns " + 
				"      ON properties_inf.predicate = " +type + "_patterns.predicate " + 
				"GROUP BY subj_Type, superPredicate, obj_Type "
			).write().option("sep", ";").csv(output_dir + "/spark-" + type + "-patternsLight");
		
	}
	
	////////////////////////////////////////////////// PREDICT CARDINALITIES ///////////////////////////////////////////////////////////////////
	
	public void predictCardinalities() {
		 session.sql("SELECT * FROM datatype_cardinalities").withColumn("predictedCardDirect",
	                callUDF("predictCardinalities", col("max"), col("avg"))).withColumn("predictedCardInverse",
	    	                callUDF("predictCardinalities", col("max2"), col("avg2")))
		 .select("AKP", "predictedCardDirect", "predictedCardInverse")
		 .write().option("sep", ";").csv(output_dir + "/spark-datatype-predictedCardinalities");	
		 
		 session.sql("SELECT * FROM object_cardinalities").withColumn("predictedCardDirect",
	                callUDF("predictCardinalities", col("max"), col("avg"))).withColumn("predictedCardInverse",
	    	                callUDF("predictCardinalities", col("max2"), col("avg2")))
		 .select("AKP", "predictedCardDirect", "predictedCardInverse")
		 .write().option("sep", ";").csv(output_dir + "/spark-object-predictedCardinalities");	
	}
	
	
	public void buildJSON(String type) {
		try {
			// read freqs
			session.read().format("csv").option("sep", ";").load(output_dir + "/spark-" + type + "-akp")
				.select(col("_c0").alias("subj"), col("_c1").alias("pred"),col("_c2").alias("obj"), col("_c3").alias("freq").cast("long"))
				.withColumn("AKP", concat(col("subj"), lit("##"),col("pred"), lit("##"), col("obj")))
				.drop("subj", "pred", "obj")
				.createOrReplaceTempView("merged");
				
			if(inference) { 
				session.read().format("csv").option("sep", ";").load(output_dir + "/spark-" + type + "-patternsLight")
					.select(col("_c0").alias("subj"), col("_c1").alias("pred"),col("_c2").alias("obj"), col("_c3").alias("numInstances").cast("long"))
					.withColumn("AKP", concat(col("subj"), lit("##"),col("pred"), lit("##"), col("obj")) )
					.createOrReplaceTempView("instances");
				
				session.sql("SELECT merged.*, instances.numInstances " + 
						"FROM instances JOIN  merged  ON merged.AKP = instances.AKP "
					).createOrReplaceTempView("merged");
			}
			if(cardinality) {
				session.read().format("csv").option("sep", ";").load(output_dir + "/spark-" + type + "-cardinalities")
					.select(col("_c0").alias("AKP"), col("_c1").alias("d_max").cast("long"), col("_c2").alias("d_avg").cast("long"), col("_c3").alias("d_min").cast("long"), 
							col("_c4").alias("i_max").cast("long"), col("_c5").alias("i_avg").cast("long"), col("_c6").alias("i_min").cast("long"))
					.createOrReplaceTempView("cards");
				
				session.sql("SELECT merged.*, cards.d_max, cards.d_avg, cards.d_min , cards.i_max , cards.i_avg , cards.i_min " + 
						"FROM cards JOIN merged ON merged.AKP = cards.AKP "
					).createOrReplaceTempView("merged");
			}
			if(shacl) {
				session.read().format("csv").option("sep", ";").load(output_dir + "/spark-" + type + "-predictedCardinalities")
					.select(col("_c0").alias("AKP"), col("_c1").alias("d_predict").cast("long") ,col("_c2").alias("i_predict").cast("long"))
					.createOrReplaceTempView("shacl");
				
				session.sql("SELECT merged.*, shacl.d_predict, shacl.i_predict " + 
						"FROM shacl JOIN merged ON merged.AKP = shacl.AKP "
					).createOrReplaceTempView("merged");
			}
			
			// split AKP columns into subjType, pred, objType
			session.sql("SELECT * FROM merged").withColumn("subjType", org.apache.spark.sql.functions.split(col("AKP"), "##").getItem(0))
				.withColumn("pred", org.apache.spark.sql.functions.split(col("AKP"), "##").getItem(1))
				.withColumn("objType", org.apache.spark.sql.functions.split(col("AKP"), "##").getItem(2))
				.drop("AKP")
				.createOrReplaceTempView("merged");
			
			session.sql("SELECT * FROM merged")
				.write().json(output_dir + "/spark-" + type + "-merged");	
			}
		catch(UnsupportedOperationException e) {
			e.printStackTrace();
			session.emptyDataFrame().write().json(output_dir + "/spark-" + type + "-merged");	
		}
	}
	
}