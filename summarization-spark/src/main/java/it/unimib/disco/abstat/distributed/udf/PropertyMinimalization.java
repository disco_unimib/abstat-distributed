package it.unimib.disco.abstat.distributed.udf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import it.unimib.disco.abstat.distributed.minimalization.PropertyGraph;


public class PropertyMinimalization extends UserDefinedAggregateFunction{


	private static final long serialVersionUID = -5678469850763524932L;
	private SparkSession session;
	private PropertyGraph propGraph;

	
	public PropertyMinimalization(String ontology_path, SparkSession session) throws Exception{
		this.session = session;
	    propGraph = new PropertyGraph(ontology_path, false);
	}
	
	
	@Override
	public StructType inputSchema() {
	    List<StructField> inputFields = new ArrayList<>();
	    inputFields.add(DataTypes.createStructField("input", DataTypes.StringType, true));
	    return DataTypes.createStructType(inputFields);
	}

	@Override
	public StructType bufferSchema() {
	    List<StructField> bufferFields = new ArrayList<>();
	    bufferFields.add(DataTypes.createStructField("bufferArray", DataTypes.createArrayType(DataTypes.StringType), true));
	    return DataTypes.createStructType(bufferFields);
	}

	@Override
	public DataType dataType() {
		return DataTypes.createArrayType(DataTypes.StringType);
	}

	@Override
	public boolean deterministic() {
		return false;
	}


	@Override
	public void initialize(MutableAggregationBuffer buffer) {
		buffer.update(0, new ArrayList<String>());
		
	}

	@Override
	public void update(MutableAggregationBuffer buffer, Row input) {
		 List<String> newValue = new ArrayList<String>();
		 newValue.addAll(buffer.getList(0));
         newValue.add(input.getString(0));
         buffer.update(0, newValue);
	}

	@Override
	public void merge(MutableAggregationBuffer buffer1, Row buffer2) {
		 List<String> newValue = new ArrayList<String>();
		 newValue.addAll(buffer1.getList(0));
		 newValue.addAll(buffer2.getList(0));
         buffer1.update(0, newValue);
	}

	@Override
	public Object evaluate(Row buffer) {
        return minimalize(buffer.getList(0));
	}
	
	public void register() {
		session.udf().register("minprop", this);
	}
	

	public Set<String> minimalize(List<String> list) {
		Set<String> set = new HashSet<String>(list);
		return propGraph.minimalize(set);
	}

}
