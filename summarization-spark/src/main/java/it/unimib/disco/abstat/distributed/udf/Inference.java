package it.unimib.disco.abstat.distributed.udf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import com.hp.hpl.jena.vocabulary.OWL;

import it.unimib.disco.abstat.distributed.minimalization.TypeGraph;

public class Inference extends UserDefinedAggregateFunction {
	
	private static final long serialVersionUID = -3379221028482431568L;
	private SparkSession session;
	private TypeGraph typeGraph;

	public Inference(String ontology_path, SparkSession session) throws Exception{
		this.session = session;
		typeGraph = new TypeGraph(ontology_path, true);
	}
	
	
	@Override
	public StructType inputSchema() {
	    List<StructField> inputFields = new ArrayList<>();
	    inputFields.add(DataTypes.createStructField("input", DataTypes.StringType, true));
	    return DataTypes.createStructType(inputFields);
	}

	@Override
	public StructType bufferSchema() {
	    List<StructField> bufferFields = new ArrayList<>();
	    bufferFields.add(DataTypes.createStructField("bufferArray", DataTypes.createArrayType(DataTypes.StringType), true));
	    return DataTypes.createStructType(bufferFields);
	}

	@Override
	public DataType dataType() {
		return DataTypes.createArrayType(DataTypes.StringType);
	}

	@Override
	public boolean deterministic() {
		return false;
	}


	@Override
	public void initialize(MutableAggregationBuffer buffer) {
		buffer.update(0, new ArrayList<String>());
		
	}

	@Override
	public void update(MutableAggregationBuffer buffer, Row input) {
		 List<String> newValue = new ArrayList<String>();
		 newValue.addAll(buffer.getList(0));
         newValue.add(input.getString(0));
         buffer.update(0, newValue);
	}

	@Override
	public void merge(MutableAggregationBuffer buffer1, Row buffer2) {
		 List<String> newValue = new ArrayList<String>();
		 newValue.addAll(buffer1.getList(0));
		 newValue.addAll(buffer2.getList(0));
         buffer1.update(0, newValue);
	}

	@Override
	public Object evaluate(Row buffer) {
        return inferTypes(buffer.getList(0));
	}
	
	public void register() {
		session.udf().register("inference", this);
	}
	

	public Set<String> inferTypes(List<String> set) {
		Set<String> types = new HashSet<String>();
		for(String concept : set) {
			Set<String> list = typeGraph.superConcepts(concept);
			if (list == null) {     //if typegraph does not contain the concept
				list = new HashSet<String>();
				list.add(concept);
				list.add(OWL.Thing.getURI());
			}
			types.addAll(list);
		}	
		return types;
	}

}
