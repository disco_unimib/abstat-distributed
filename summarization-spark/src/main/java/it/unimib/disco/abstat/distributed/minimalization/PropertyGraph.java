package it.unimib.disco.abstat.distributed.minimalization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDFS;


/* This graph contains only properties which are represented inside the ontology (explicit definition, domain, range  and subProperty statements). 
** Therefore external properties are not added to the graph. Notice that it's also possible to add two theoretical properties to the graph and link
*  every property in the ontology to one of these
*/
public class PropertyGraph implements Serializable {

	private static final long serialVersionUID = -5177180828628430682L;
	DirectedAcyclicGraph<String, DefaultEdge> graph = new DirectedAcyclicGraph<String, DefaultEdge>(DefaultEdge.class);
	static OntModel ontologyModel;
	
	public PropertyGraph(String ontology, boolean linkToTheoreticalProps) {
		ontologyModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM, null);
		ontologyModel.read(ontology, "RDF/XML");	
		populateVertex(true);
		populateEdges();
		
		if(linkToTheoreticalProps)
			linkToTheoreticalProperties();
	}
	
	
//////////////////////////////////GRAPH POPULATION ///////////////////////////////////////////////////////
	
	
	private void populateVertex(boolean enrich) {
		if(enrich)
			enrichWithImplicitPropertyDeclarations();
		ExtendedIterator<OntProperty> TempExtractedPropery = ontologyModel.listAllOntProperties();
		while(TempExtractedPropery.hasNext()) {
			String URI = TempExtractedPropery.next().getURI();
			if(URI!=null && !URI.equals("")) 
				graph.addVertex(URI);			
		}
		TempExtractedPropery.close();
	}
	
	
	private void populateEdges() {
		ExtendedIterator<OntProperty> itProps = ontologyModel.listAllOntProperties();
		while(itProps.hasNext()) {
			OntProperty prop = itProps.next();
			String v = prop.getURI();
			
			try {
				ExtendedIterator<OntProperty> it = (ExtendedIterator<OntProperty>) prop.listSuperProperties(true);	
				while(it.hasNext()) {
					String vSup = it.next().getURI();
					if(vSup!=null)
						graph.addEdge(v, vSup);
				}
			}
			catch(Exception e) {
				String queryString = "SELECT ?obj " +
									 "WHERE { <" + v + "> <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?obj }";
				
				Query query = QueryFactory.create(queryString) ;
				QueryExecution qexec = QueryExecutionFactory.create(query, ontologyModel) ;
				
				try {
					ResultSet results = qexec.execSelect();
					
				    for ( ; results.hasNext() ; ){
				    	QuerySolution soln = results.nextSolution() ;
				    	String vSup = soln.getResource("obj").getURI();
			//	    	if(vSup!=null && v!=vSup && !graph.containsVertex(vSup))         //forse da eliminare 
			//	    		graph.addVertex(vSup);                                       
				    	graph.addEdge(v, vSup);				
				    }
				} 
				finally { qexec.close() ; }
			}		
		}	
	}
	
	// extract properties from implicit ontology declarations
	private void enrichWithImplicitPropertyDeclarations() {
		StmtIterator statements = ontologyModel.listStatements(new SimpleSelector(){
			@Override
			public boolean selects(Statement s) {
				Property predicate = s.getPredicate();
				return predicate.equals(RDFS.domain) || predicate.equals(RDFS.range);
			}
		});
		HashSet<String> implicitProperties = new HashSet<String>();
		while(statements.hasNext())
			implicitProperties.add(statements.next().getSubject().getURI());
		statements.close();
		
		statements = ontologyModel.listStatements(new SimpleSelector(){
			@Override
			public boolean selects(Statement s) {
				Property predicate = s.getPredicate();
				return predicate.equals(RDFS.subPropertyOf);
			}
		});
		
		while(statements.hasNext()){
			Statement statement = statements.next();
			implicitProperties.add(statement.getSubject().getURI());
			implicitProperties.add(statement.getObject().toString());
		}
		statements.close();
		
		for(String property : implicitProperties)
			ontologyModel.createOntProperty(property);
	}

	
	public void linkToTheoreticalProperties() {
		String topDataProperty = "http://www.w3.org/2002/07/owl#topDataProperty";
		String topObjectProperty = "http://www.w3.org/2002/07/owl#topObjectProperty";
		graph.addVertex(topDataProperty);
		graph.addVertex(topObjectProperty);

		for (String prop : findRoots()) {
			if (!prop.equals(topDataProperty) && !prop.equals(topObjectProperty)) {
				if (ontologyModel.getOntProperty(prop).isDatatypeProperty())
					graph.addEdge(prop, topDataProperty);
				else
					graph.addEdge(prop, topObjectProperty);
			}
		}
	}	
	
	////////////////////////////////// OPERATIONS ///////////////////////////////////////////////////////
	
	public List<List<String>> pathsBetween(String v1, String v2){
		String vertex = v1;
		String orfano = v2;
		
		ArrayList<List<String>> paths = new ArrayList<List<String>>();
		if(graph.containsVertex(vertex) && graph.containsVertex(orfano)){
			inOrderTraversal(vertex, orfano, new ArrayList<String>(), paths);
		}
		return paths;
	}
	
	
	private void inOrderTraversal(String vertex, String orfano, List<String> currentPath, ArrayList<List<String>> paths){
		ArrayList<String> path = new ArrayList<String>(currentPath);
		path.add(vertex);
		if(vertex.equals(orfano)){
			paths.add(path);
		}
		for(DefaultEdge edgeToSuperType : graph.outgoingEdgesOf(vertex)){
			String superType = graph.getEdgeTarget(edgeToSuperType);
			inOrderTraversal(superType, orfano, path, paths);
		}
	}
	
	
	// if p is present in the graph it returns the superproperties (transitive closure) plus the input property, otherwise returns null
	public Set<String> superProperties(String v) {
		if (!graph.containsVertex(v)) 
			return null;
		else {
			Set<String> set = new HashSet<String>();
			Set<DefaultEdge> outgoingEdges = graph.outgoingEdgesOf(v);

			for (DefaultEdge edge : outgoingEdges) {
				String padre = graph.getEdgeTarget(edge);
				String source = graph.getEdgeSource(edge);
				Set<String> gerarchia = superProperties(padre);
				set.add(source);
				set.addAll(gerarchia);
			}
			if (outgoingEdges.isEmpty()) 
				set.add(v);
			return set;
		}
	}
	
	
	// returns a set of vertexes without father
	public HashSet<String> findRoots() {
		HashSet<String> orphans = new HashSet<String>();

		for (String vertex : graph.vertexSet()) {
			boolean isOrphan = true;
			for (DefaultEdge edge : graph.edgesOf(vertex)) {
				if (graph.getEdgeSource(edge).equals(vertex))
					isOrphan = false;
			}
			if (isOrphan)
				orphans.add(vertex);
		}
		return orphans;
	}
	
	
	// given a set of properties it returns a set with only minimal properties
	public Set<String> minimalize(Set<String> set) {
		Set<String> minimalProps = new HashSet<String>();
		String firstMinimalProp = (String) set.toArray()[0];
		minimalProps.add(firstMinimalProp);
		set.remove(firstMinimalProp);
		
		boolean minimal= true;
		for(String concept: set) {
			Iterator<String> it = minimalProps.iterator();
			while(it.hasNext()) {
				String minimalType = it.next();
				if(!pathsBetween(minimalType, concept).isEmpty())
					minimal = false;
				if(!pathsBetween(concept, minimalType).isEmpty())
					it.remove();
			}
			if(minimal)
				minimalProps.add(concept);
			minimal = true;
		}
		return minimalProps;
	}
	
}
