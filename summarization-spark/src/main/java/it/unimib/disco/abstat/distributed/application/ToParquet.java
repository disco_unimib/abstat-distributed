package it.unimib.disco.abstat.distributed.application;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import it.unimib.disco.abstat.distributed.minimalization.Triple;


/*
 * Takes a master, a comma-separated list of .nt datasets and an output_dir on HDFS.
 * Regardless the number of input datasets , the output is always a directory with the .parquet files
 * we recommend to include the word "parquet" in the output_dir argument
 */
public class ToParquet {

	private SparkSession session;
	
	public ToParquet(String master) {
		session = SparkSession.builder().appName("ABSTAT-spark").master(master).getOrCreate();
	}
	
	
	public static void main(String[] args) throws Exception {
		ToParquet t = new ToParquet(args[0]);
		String[] datasets = args[1].split(",");
		String output_file = args[2];
		t.convert(datasets, output_file);	
	}
	
	
	public void convert(String[] datasets, String output_file) throws Exception {
		JavaRDD<String> input = session.read().textFile(datasets).javaRDD();
		JavaRDD<Triple> rdd = new Splitter().calculate(input);
		Dataset<Row> data = session.createDataFrame(rdd, Triple.class);
		data.write().format("parquet").mode("append").save(output_file);
	}
}
