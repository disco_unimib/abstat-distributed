package it.unimib.disco.abstat.distributed.udf;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.types.DataTypes;

public class PredictCardinalities {

	private SparkSession session;
	
	public PredictCardinalities(SparkSession session) {
		this.session = session;
	}
	
	public void register() {
		session.udf().register("predictCardinalities", (UDF2<Long, Integer, Long>)
	            (maxCol, avgCol) -> {
	//            	long max = Long.parseLong(maxCol);
	  //          	long avg = Long.parseLong(avgCol);
	            	long maxPredicted = maxCol > 2 * avgCol ? 2 * avgCol : maxCol;
	            	return maxPredicted;
	            }, DataTypes.LongType);
	}
}
