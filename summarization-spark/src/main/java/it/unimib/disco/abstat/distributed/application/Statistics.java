package it.unimib.disco.abstat.distributed.application;

import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.max;
import static org.apache.spark.sql.functions.min;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;

import it.unimib.disco.abstat.distributed.minimalization.Triple;
import it.unimib.disco.abstat.distributed.udf.Minimalize;


public class Statistics {
	private SparkSession session;
	private String output_dir;

	public Statistics(String master, String output_dir) {
		session = SparkSession.builder().appName("ABSTAT-spark").master(master).getOrCreate();
		this.output_dir = output_dir;
	}
	
	
	public static void main(String[] args) throws Exception {
		Statistics s = new Statistics(args[0], args[3]);
		String[] datasets = args[1].split(",");
		String ontology = args[2];
		s.preProcessing(datasets);
		s.regUDF(ontology);
		
		s.split();
		s.dataset_stats();
		//s.calculateMinimalTypes();
		//s.summary_stats();
	}
		
	
	
	public void preProcessing(String[] datasets) throws Exception {
		JavaRDD<String> input = session.read().textFile(datasets).javaRDD();
		JavaRDD<Triple> rdd = new Splitter().calculate(input);
		Dataset<Row> data = session.createDataFrame(rdd, Triple.class);
		data.createOrReplaceTempView("dataset");
	}
	
	
	
	public void regUDF(String ontology) throws Exception {
		new Minimalize(ontology, session).register();
	}
	
	
	public void split() {
		/* isolate typing asserts */
		session.sql("SELECT DISTINCT subject, object " + 
					"FROM dataset "  + 
					"WHERE type = 'typing'"
				   ).createOrReplaceTempView("typing_triples");	
		
		/* isolate object relation asserts */
		session.sql("SELECT DISTINCT subject, predicate, object " + 
					"FROM dataset "  + 
					"WHERE type = 'object_relational'" 
				   ).createOrReplaceTempView("object_triples");
		
		/* isolate datatype relation asserts */
		session.sql("SELECT DISTINCT subject, predicate, object, " +                     
					"   CASE" + 
					"      WHEN datatype IS NULL THEN 'http://www.w3.org/2000/01/rdf-schema#Literal' " + 
					"      ELSE datatype " + 
					"   END AS datatype " +
					"FROM dataset "  + 
					"WHERE type = 'dt_relational'" 
				   ).createOrReplaceTempView("dt_triples");	
	}
	 
	
	public void calculateMinimalTypes() {
		/* minimal types per entity */
		session.sql("SELECT subject AS entity, minimalize(object) AS minimalTypes " + 
					"FROM typing_triples " + 
					"GROUP BY subject"
					).createOrReplaceTempView("mTypes_dataset");
		
		/* minimal type per entity */
		session.sql("SELECT entity, explode(minimalTypes) AS minimalType " + 
					"FROM mTypes_dataset"
					).createOrReplaceTempView("mTypes_dataset");
	}
	
	
	public void dataset_stats() {
		// number of typing assertions
		session.sql("SELECT COUNT(*)  FROM typing_triples"
				).write().option("header", "true").option("sep", ";").csv(output_dir + "/count_typing_assertions");
		
		
		// number of object relational assertions
		session.sql("SELECT COUNT(*)  FROM object_triples"
				).write().option("header", "true").option("sep", ";").csv(output_dir + "/count_object_rel_assertions");
		
		
		// number of datattype relational assertions
		session.sql("SELECT COUNT(*)  FROM dt_triples"
				).write().option("header", "true").option("sep", ";").csv(output_dir + "/count_datatype_rel_assertions");
		
		
		// number of entities intended as such entities about which the dataset is says something 
		session.sql("SELECT COUNT (DISTINCT subject) " +
				    "FROM dataset " 
				  ).write().option("header", "true").option("sep", ";").csv(output_dir + "/count_entities");
		
		
		// total entites (even that ones that are not subjects)
	/*	session.sql("SELECT COUNT(*) FROM (" +
		            "	SELECT subject FROM dataset " +
			        "	UNION " + 
				    "	SELECT object FROM object_triples" +
			        ")"
			  ).write().option("header", "true").option("sep", ";").csv(output_dir + "/count_entities"_all); */
		

		// total min/average/max types per entity
		session.sql("SELECT subject, count(*) AS count " + 
				"FROM typing_triples "  + 
				"GROUP BY subject " 
			   ).agg(max("count").alias("max_types"), avg("count").alias("avg_types"), min("count").alias("min_types"))
				.write().option("header", "true").option("sep", ";").csv(output_dir + "/count_types_per_entity");
	}


	
	public void summary_stats() {

		/* statistics about minimal types per entity */
		session.sql("SELECT entity, count(*) AS count " + 
					"FROM mTypes_dataset "  + 
					"GROUP BY entity " 
				   ).agg(max("count").alias("max_minTypes"), avg("count").alias("avg_minTypes"), min("count").alias("min_minTypes"))
		.createOrReplaceTempView("minimalTypesPerEntity");
		
		session.sql("SELECT * FROM "
				  + "typesPerEntity CROSS JOIN  minimalTypesPerEntity"
			   ).write().option("header", "true").option("sep", ";").csv(output_dir + "/types");
	
	
		/* statistics about entities in datatype relational assertions */
		session.sql("SELECT * FROM "
				  + "( "
			      + "	SELECT count(subject) AS subjects"
				  + "	FROM dt_triples "
				  + ") "
				  + "CROSS JOIN ( "
			      + "	SELECT count(DISTINCT subject) AS distinct_subjects"
				  + "	FROM dt_triples "
				  + ") " 
			   ).write().option("header", "true").option("sep", ";").csv(output_dir + "/datatype_relations");
		
		
		/* statistics about entities in object relational assertions */
		session.sql("SELECT * FROM "
				  + "( "
			      + "	SELECT count(subject) AS subjects"
				  + "	FROM object_triples "
				  + ") "
				  + "CROSS JOIN ( "
				  + "	SELECT  count (object) AS objects" + 
				  "		FROM object_triples "
				  + ")"  
				  + "CROSS JOIN ( "
			      + "	SELECT count(DISTINCT subject) AS distinct_subjects"
				  + "	FROM object_triples "
				  + ") "
				  + "CROSS JOIN ( "
				  + "	SELECT  count (DISTINCT object) AS distinct_objects " + 
				  "		FROM object_triples "
				  + ")"  
			   ).write().option("header", "true").option("sep", ";").csv(output_dir + "/object_relations");
	}
}
