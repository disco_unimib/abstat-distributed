function build(){
	cd summarization-spark
	mvn package --quiet
	echo "spark app builded"
}


relative_path=`dirname $0`
cd $relative_path

case "$1" in
	build)
		build
		;;
	*)
	echo "Usage: abstat build "
		;;
esac
