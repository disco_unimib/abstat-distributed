
# ABSTAT-HD: a Scalable Tool for Profiling Very Large Knowledge Graphs

ABSTAT-HD is a distributed tool for profiling very large Knowledge Graphs (KGs), offering semantic summarization, statistics computation, and scalability over distributed computing infrastructures.

---

## Features
- Computes concise semantic profiles for large-scale KG using Abstract Knowledge Patterns (AKPs).
- Distributed architecture for processing very large datasets efficiently.

--- 

## Prerequisites
1. **Dependencies**:  
   - Java 8+
   - Apache Maven
   - Spark 2.x
2. **Input Requirements**:  
   - Dataset in RDF format.
   - Ontology file in OWL format (optional).

---

## Setup and Build
1. Clone the repository and navigate to the project directory:
   ```bash
   git clone <repo-url>
   cd summarization-spark
   b```
2. uild the project:
   ```bash
   ./abstat.sh build
   ```
   This script compiles the source code and packages it into a JAR file.

---

## Running a Job
Use the `launch_job.sh` script to execute summarization in either distributed or local mode:

### Script Parameters:
1. **`dataset`**: Path to the RDF dataset.
2. **`ontology`**: Path to the ontology file.
3. **`output_dir`**: Directory for storing results.
4. **`isDistributed`**: Set to `true` for distributed mode, otherwise `false`.
5. **`propMin`**: Enable property minimalization? (`true`/`false`)
6. **`inf`**: Enable inference (`true`/`false`).
7. **`card`**: Include cardinality statistics (`true`/`false`).
8. **`shacl`**: Enable SHACL-based validation (`true`/`false`).

### Example Command
```bash
./launch_job.sh dataset.rdf ontology.owl output/ true true true true false
```


### Output Files:
- Results are merged into JSON and TXT files summarizing patterns, cardinalities, and inferred statistics.

---

## References

For a detailed explanation of ABSTAT's methodologies and applications, refer to the following papers:

- *Principe, R. A. A., Spahiu, B., Palmonari, M., Rula, A., De Paoli, F., & Maurino, A. (2018). ABSTAT 1.0: Compute, manage and share semantic profiles of RDF knowledge graphs. In The Semantic Web: ESWC 2018 Satellite Events: ESWC 2018 Satellite Events, Heraklion, Crete, Greece, June 3-7, 2018, Revised Selected Papers 15 (pp. 170-175). Springer International Publishing.*
- *Alva Principe, R. A., Maurino, A., Palmonari, M., Ciavotta, M., & Spahiu, B. (2021). ABSTAT-HD: a scalable tool for profiling very large knowledge graphs. The VLDB Journal, 1-26.*
---


## License

GNU Affero General Public License v3.0
